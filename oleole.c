#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

int cantidadDeVeces = 5;
sem_t sem1;
sem_t sem2;
sem_t sem3;
sem_t sem4;
sem_t sem5;

sem_t aux;


void* funcion_num1(){

int fun1 = cantidadDeVeces;
	while(fun1>0){
	int i = 0;
	fun1--;
		while(i<2){	
		sem_wait(&sem1);

		printf("ole ole ole\n");
		
		i++;
		sem_post(&aux);
		}
	}
}

void* funcion_num2(){

int fun2 = cantidadDeVeces;

	while(fun2>0){
	sem_wait(&sem2);
	sem_wait(&aux);

	printf("ole ole ole olá\n");
	
	fun2--;	
	sem_post(&sem3);
	sem_post(&sem1);
	}
}

void* funcion_num3(){

int fun3 = cantidadDeVeces;
	
	while(fun3>0){
	sem_wait(&sem3);
	sem_wait(&aux);

	printf("cada dia te quiero más\n");
	
	fun3--;
	sem_post(&sem2);
	sem_post(&sem4);
	}
}

void* funcion_num4(){

int fun4 = cantidadDeVeces;

	while(fun4>0){
	sem_wait(&sem4);

	printf("oooh Argentina\n");

	fun4--;
	sem_post(&sem5);
	}
}

void* funcion_num5(){

int fun5 = cantidadDeVeces;
	
	while(fun5>0){
	sem_wait(&sem5);

	printf("es un sentimiento no puedo parar!!\n");

	fun5--;
	sem_post(&sem1);
	}
}


int main(){

//Inicializo semaforos
sem_init(&sem1,0,1);
sem_init(&sem2,0,1);
sem_init(&sem3,0,0);
sem_init(&sem4,0,0);
sem_init(&sem5,0,0);

sem_init(&aux,0,0);

//Creo Threads
pthread_t p1;
pthread_t p2;
pthread_t p3;
pthread_t p4;
pthread_t p5;

//Lanzo threads
int rc;

rc = pthread_create(&p1,NULL,funcion_num5,NULL);
rc = pthread_create(&p2,NULL,funcion_num1,NULL);
rc = pthread_create(&p3,NULL,funcion_num3,NULL);
rc = pthread_create(&p4,NULL,funcion_num2,NULL);
rc = pthread_create(&p5,NULL,funcion_num4,NULL);
 
pthread_join(p1 , NULL);
pthread_join(p2 , NULL);
pthread_join(p3 , NULL);
pthread_join(p4 , NULL);
pthread_join(p5 , NULL);

pthread_exit(NULL);

sem_destroy(&sem1);
sem_destroy(&sem2);
sem_destroy(&sem3);
sem_destroy(&sem4);
sem_destroy(&sem5);
sem_destroy(&aux);

return 0;
}
