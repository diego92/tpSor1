#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

int cantidadDeVeces = 10;
sem_t sem1;
sem_t sem2;
sem_t sem3;

void* funcion_MA(){

int fun1 = cantidadDeVeces;
	while(fun1>0){
	sem_wait(&sem1);

	printf("MA");

	fun1--;
	sem_post(&sem2);
	}
}

void* funcion_RA(){

int fun2 = cantidadDeVeces;
	while(fun2>0){
	sem_wait(&sem2);

	printf("RA");

	fun2--;
	sem_post(&sem3);
	}

}

void* funcion_DO(){

int fun3 = cantidadDeVeces;
	while(fun3>0){
	sem_wait(&sem3);

	printf("DOOO...\n");

	fun3--;
	sem_post(&sem1);
	}

}


int main(){

sem_init(&sem1,0,1);
sem_init(&sem2,0,0);
sem_init(&sem3,0,0);

pthread_t p1;
pthread_t p2;
pthread_t p3;

int rc;

rc = pthread_create(&p1,NULL,funcion_MA,NULL);
rc = pthread_create(&p2,NULL,funcion_RA,NULL);
rc = pthread_create(&p3,NULL,funcion_DO,NULL);
 
 pthread_join(p1 , NULL);
 pthread_join(p2 , NULL);
 pthread_join(p3 , NULL);

	pthread_exit(NULL);

sem_destroy(&sem1);
sem_destroy(&sem2);
sem_destroy(&sem3);

return 0;
}
