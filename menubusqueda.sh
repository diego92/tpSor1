#!/bin/bash
#------------------------------------------------------
# PALETA DE COLORES
#------------------------------------------------------
#setaf para color de letras/setab: color de fondo
    red=`tput setaf 1`;
    green=`tput setaf 2`;
    blue=`tput setaf 4`;
    bg_blue=`tput setab 4`;
    reset=`tput sgr0`;
    bold=`tput setaf bold`;
#------------------------------------------------------
# VARIABLES GLOBALES
#------------------------------------------------------


#------------------------------------------------------
# DISPLAY MENU
#------------------------------------------------------
imprimir_menu () {
       imprimir_encabezado "\t  S  U  P  E  R  -  M  E  N U ";
    
    echo -e "\t\t Menu 2 de busquedas";
    
    echo -e "\t\t";
    echo -e "\t\t Opciones:";
    echo "";
    echo -e "\t\t\t a.  Buscar si un programa esta instalado";
    echo -e "\t\t\t b.  Buscar en directorio archivos con una extension";
    echo -e "\t\t\t c.  Busca en archivo la linea donde se encuentra un string";
    echo -e "\t\t\t d.  Calendario";             
    echo -e "\t\t\t q.  Salir";
    echo "";
    echo -e "Escriba la opción y presione ENTER";
}

#------------------------------------------------------
# FUNCTIONES AUXILIARES
#------------------------------------------------------

imprimir_encabezado () {
    clear;
    #Se le agrega formato a la fecha que muestra
    #Se agrega variable $USER para ver que usuario está ejecutando
    echo -e "`date +"%d-%m-%Y %T" `\t\t\t\t\t USERNAME:$USER";
    echo "";
    #Se agregan colores a encabezado
    echo -e "\t\t ${bg_blue} ${red} ${bold}--------------------------------------\t${reset}";
    echo -e "\t\t ${bold}${bg_blue}${red}$1\t\t${reset}";
    echo -e "\t\t ${bg_blue}${red} ${bold} --------------------------------------\t${reset}";
    echo "";
}

esperar () {
    echo "";
    echo -e "Presione enter para continuar";
    read ENTER ;
}

malaEleccion () {
    echo -e "Selección Inválida ..." ;
}

decidir () {
    echo $1;
    while true; do
        echo "desea ejecutar? (s/n)";
            read respuesta;
            case $respuesta in
                [Nn]* ) break;;
                   [Ss]* ) eval $1
                break;;
                * ) echo "Por favor tipear S/s ó N/n.";;
            esac
    done
}

#------------------------------------------------------
# FUNCTIONES del MENU
#------------------------------------------------------
a_funcion () {
        imprimir_encabezado "\tOpción a.  Ver si el programa esta instalado";
	echo -e "Escriba el nombre del programa que desea buscar: "
	read prog        
	dpkg -l | grep $prog
}

b_funcion () {
           imprimir_encabezado "\tOpción b.  Buscar en directorio archivos con una extension";
		echo -e "Ingrese un directorio valido: "
		read direc
		echo -e "Ingrese una extension de archivo: "
		read extension
		echo -e "Ingrese un nombre de archivo: "
		read nombre
		find $direc -type f -name "*.$extension" | grep -i $nombre
		
}

c_funcion () {
          imprimir_encabezado "\tOpción c.  Busca en archivo la linea donde se encuentra un string";
	echo "Ingrese un string: ";
	read string;
	echo "Ingrese la direccion(path) de un archivo txt: ";
	read pathdearchivo
	cat $pathdearchivo | grep -n $string >> salida.txt
		      
}

d_funcion () {
    imprimir_encabezado "\tOpción d.  Calendario";
	echo "Ingrese un nombre para el proceso de cambio de estados: ";
	read nomb;
	gcc estadosDeProceso.c -o $nomb;
	top | grep $nomb;
	
}


#------------------------------------------------------
# LOGICA PRINCIPAL
#------------------------------------------------------
while  true
do
    # 1. mostrar el menu
    imprimir_menu;
    # 2. leer la opcion del usuario
    read opcion;
    
    case $opcion in
        a|A) a_funcion;;
        b|B) b_funcion;;
        c|C) c_funcion;;
        d|D) d_funcion;;
        e|E) e_funcion;;
        q|Q) break;;
        *) malaEleccion;;
    esac
    esperar;
done
 

